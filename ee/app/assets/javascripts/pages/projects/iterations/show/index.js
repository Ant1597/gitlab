import { initIterationReport } from 'ee/iterations';

document.addEventListener('DOMContentLoaded', initIterationReport);
